let userName = prompt("Enter your Username: ");
let userPassword = prompt("Enter your Password: ");
let userRole = prompt("Enter your Role: ").toLowerCase();

if(userName==="" || userName===null || userPassword==="" || userPassword===null || userRole==="" || userRole===null){
    alert ("Input should not be empty"); 
} 

switch(userRole) {
    case "admin":
        alert("Welcome back to the class portal, admin!");
        alert("ADMIN! You are not allowed to access this feature!");
        break;
    case "teacher":
        alert("Thank you for logging in, teacher!");
        alert("TEACHER! You are not allowed to access this feature!");
        break;
    case "student":
        alert("Welcome to the class portal, student!")
        break;
    default:
        alert("Role out of range.")
        alert("You are not allowed to access this feature!");
        break;
}



function checkAverage(grade1, grade2, grade3, grade4){
    let finalGradeAverage = (grade1 + grade2 + grade3 + grade4) / 4;
    let gradePercentage = (Math.round(finalGradeAverage));

    if(gradePercentage <= 74) {
        console.log("Hello, student, your average is: " + gradePercentage + " The letter equivalent is F");
    } else if (gradePercentage >=75 && gradePercentage <=79) {
        console.log("Hello, student, your average is: " + gradePercentage + " The letter equivalent is D")
    } else if (gradePercentage >=80 && gradePercentage <=84) {
        console.log("Hello, student, your average is: " + gradePercentage + " The letter equivalent is C")
    } else if (gradePercentage >=85 && gradePercentage <=89) {
        console.log("Hello, student, your average is: " + gradePercentage + " The letter equivalent is B")
    } else if (gradePercentage >=90 && gradePercentage <=95) {
        console.log("Hello, student, your average is: " + gradePercentage + " The letter equivalent is A")
    } else if (gradePercentage >=96) {
        console.log("Hello, student, your average is: " + gradePercentage + " The letter equivalent is A+")
    }

}

checkAverage();


/*

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/